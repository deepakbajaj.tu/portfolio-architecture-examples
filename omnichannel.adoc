= Omnichannel Customer Experience
Eric D. Schabell @eschabell
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left
:toclevels: 5

_Some details will differ based on the requirements of a specific implementation but all portfolio architectures generalize one or more successful deployments of a use case._


*Use case:* Omnichannel implies integration and orchestration of channels such that the experience of engaging across
all the channels someone chooses to use.

*Background:* An omnichannel approach provides a unified customer experience across platforms, creating a single view for
customers to interact with their own information.

== Solution overview

Red Hat provides a foundation for IT teams to develop and deliver omnichannel services through a combination
of integration and process automation technologies. Agile integration defines how organizations are transforming
and delivering on their digital promise to customers by integrating applications and services across on-premise
infrastructure and cloud environments. Business automation, as process integrations, are captured to
enable access to complex process services.

====
*Omnichannel Customer Experience*

. Data management, security, and user access
. Multiple protocol support through different integration technologies
. Distributed deployments, non-centralized integration
====


--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/omnichannel-marketing-slide.png[750,700]
--

== Logical diagram
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/omnichannel-customer-experience-details-ld.png[750,700]
--

== The technology

The following technology was chosen for this solution:

====
https://www.redhat.com/en/products/integration?intcmp=7013a00000318EWAAY[*Red Hat Integration*] Manage APIs. Share, secure, distribute, control, and monetize
APIs as connecting endpoint from Web UI, Mobile applications and third party. Frameworks and connectors to integrate
consumer applications with backend systems or third party services. With support for connecting both API and real-time
data streams.

https://www.redhat.com/en/technologies/cloud-computing/openshift-data-foundation?intcmp=7013a00000318EWAAY[*Red Hat OpenShift Data Foundations*] software-defined storage for containers. For real-time data storage and analysis,
realizations of logical storage definitions as needed by applications, processes or services. https://www.redhat.com/en/technologies/cloud-computing/openshift/data-foundation/trial?intcmp=7013a000003Sh3TAAS[*Try It >*]

https://www.redhat.com/en/products/runtime?intcmp=7013a00000318EWAAY[*Red Hat OpenShift Runtimes*] Foundation to build microservices. Support cloud native development strategy with built-in
build and deployment support. Also includes Single Sign On solution that can  be tied into existing organizational
directories.

https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it?intcmp=7013a00000318EWAAY[*Red Hat OpenShift*] Kubernetes container platform for orchestrating, managing, handling deployments, auto scaling of
the containerized application. https://www.redhat.com/en/technologies/cloud-computing/openshift/ocp-self-managed-trial?intcmp=7013a000003Sh3TAAS[*Try It >*]

https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux?intcmp=7013a00000318EWAAY[*Red Hat Enterprise Linux*] is the world’s leading enterprise Linux platform. It’s an open source operating system
(OS). It’s the foundation from which you can scale existing apps—and roll out emerging technologies—across bare-metal,
virtual, container, and all types of cloud environments. https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux/server/trial?intcmp=7013a000003Sh3TAAS[*Try It >*]
====

== Architectures
=== Process integration example
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/omnichannel-process-integration-sd.png[750,700]
--

Example of a process application deployed in a mobile applications making calls through the API Gateway to leverage
both Frontend Microservices and Process Facade Microservices to access functionality in the Process Server and
integration with backend systems through the Integration Microservices. Container Native Storage shown used for process
storage as an example. Not showing monitoring.


=== Mobile integration example
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/omnichannel-mobile-integration-sd.png[750,700]
--

Example of a mobile application making calls through the API Gateway to leverage both Frontend Microservices and Mobile
Services to serve data to the device and integration with backend systems through the Integration Microservices.
Container Native Storage shown as the data source for mobile data consumption in this example for simplicity.

=== Integration service example
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/omnichannel-integration-service-sd.png[750,700]
--

Example use of integration microservices with web ui making calls through the API Gateway to leverage Frontend
Microservices that in turn call to various integration with backend systems through an Integration Microservice. SSO
server shown with integration to existing company backend Active Directory Server for authentication. Not showing
monitoring.

=== Integration data service example
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/omnichannel-integration-data-service-sd.png[750,700]
--

Example use of integration microservices with web ui making calls through the API Gateway to leverage Frontend
Microservices that in turn call to various integration with a customer contact database through an Integration Data
Microservice. SSO server shown with integration to existing company backend Active Directory Server for authentication.
Not showing monitoring.

=== Integration third-party services
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/omnichannel-process-integration-3rd-party-services-sd.png[750,700]
--

Example use of integration microservices with web ui making calls through the API Gateway to leverage Frontend
Microservices that in turn call to various integration with third-party service through an Integration Microservice.
SSO server shown with integration to existing company backend Active Directory Server for authentication. Not showing
monitoring.

== Download diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/omnichannel-customer-experience.drawio[[Open Diagrams]]
--

== Provide feedback 
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/omnichannel.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].
